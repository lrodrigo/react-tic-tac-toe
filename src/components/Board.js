import React from 'react';
import Square from './Square';

class Board extends React.Component {

    renderSquare(i) {
        return <Square key={i} value={this.props.squares[i]} onClick={() => this.props.onClick(i)}/>;
    }

    render() {
      const numberOfColumns = 3;
      const numberOfRows = 3;
      let squareNumber = 0;

      let table= [];

      for ( let i = 0; i < numberOfRows; i += 1 ) {
        let children = [];
        for ( let j = 0; j < numberOfColumns; j += 1 ) {
          children.push(this.renderSquare(squareNumber++));
        }
        table.push(<div key={i} className="board-row">{children}</div>);
      }


        return (
            <div>
                {table}
            </div>
        );
    }
}

export default Board;
